#Exercise 2: Write another program that prompts for a list of numbers
#as above (ch5_ex1.py) and at the end prints out both the maximum and minimum of
#the numbers instead of the average.
#from typing import Any, Union

counter = 0
print('Enter a random number:')

while True:                             # A loop that will run until a defined condition is not met, in this situation "done"
    message = input('*')
    if message == 'done':               # writing 'done' will execute an output
        break                           # will continue the program to run, even if a letter is written
    try:                                # try statement will print a error if the input is NOT a number or 'done'
        value = float(message)
    except:
        print('Please enter a number, or write "done" to stop the program ')
        continue
    if counter == 0:                    # set the first count to 0
        max_value = value               # defines mix_value
        min_value = value               # defines min_value

    elif value < min_value:             # if the new value is smaller than the current min_value, the value of min_value will change to the new lowest value
        min_value = value

    elif value > max_value:             # if the new value is bigger than the current max_value, the value of max_value will change to the new biggest value
        max_value = value

    counter = counter + 1               # line 19-20 will only be executed 1 time, because it will start counting for every input from the user
print("Max value:", max_value, "Min value:",min_value)
