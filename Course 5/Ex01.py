#Exercise 1: Write a program which repeatedly reads numbers until the
#user enters “done”. Once “done” is entered, print out the total, count,
#and average of the numbers. If the user enters anything other than a
#number, detect their mistake using try and except and print an error
#message and skip to the next number.

total = 0
counter = 0

print('Enter a random number:')

while True:                             # A loop that will run until a defined condition is not met, in this situation "done"
    message = input('*')
    if message == 'done':               # writing 'done' will execute an output and end the code
        break                           # will continue the program to run, even if a letter is written
    try:                                # try statement will print a error if the input is NOT a number or 'done'
        value = float(message)
    except:
        print('Please enter a number, or "done" to stop the program ')
        continue
    total = total + value
    counter = counter + 1
print("Sum:",total,"Total inputs:", counter,"Average", total/counter)    # gives the output. Gives the sum, counting input and the average of the sum.

print()


