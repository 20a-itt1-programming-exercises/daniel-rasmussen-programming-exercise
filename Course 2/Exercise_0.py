*What types of values is Python using?
Numbers - Uses numbers variable ( var = 25 )
String - Uses string variable by enclosing characters in qoutes ( "Hey" )
List - Uses a list of a series of values ( [1, 5, 10] ) or ( [Tiger, dog, bird] )
Tuple - Uses a group of values ( ('Tiger', 'Cat', 'Bird' ) )
Dictionary - Is a list of key:value pairs. Puts a value based on the key name {'John': 200, 'Tom' : 100}



*How would you check a values type in a Python program?

Must start with a letter or underscore
Must consist of letters, numbers and underscores
For instance: spam   eggs   spam23  



*What are variables?

Variable is a named place in the memory where a programmer can store data and later retrieve the data using the variable "name" You can change the contents of a variable in a later statement

x = 12.2 - is saying that x is called 12.2 but we can change it to 100, then x = 100

a = 10
b = 12.5
c = a * b
print(c) = 125


*What are reserved words?

These words can't be used as variable names / identifiers



*What is a statement?

We assign a value to a variable using the assignment statement (=)

a = 6 <-- Assignment statement
a = a + 2  <-- Assignment with expression
print(a)  <-- Print statement