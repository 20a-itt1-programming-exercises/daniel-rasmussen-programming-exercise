
*What the Python interpreter is and how it differs from a compiler?

Interpreter: Is converting a program statement into a machine code 1 by 1 while the program is running

Compiler: A compilers job is to translate a program language into machine code.


*What is the difference between syntax errors, logic errors and semantic errors?


Syntax relate to spelling and grammar. Logic relate to program flow. Semantics relate to meaning and context


*What is a program?


A computer program is a collection of instructions that can be executed by a computer to perform a specific task. A computer program is usually written by a computer programmer in a programming language (machine code).

*What is input and output?

Input Device. A input device is what we are telling the computer to do. For instance. We have a keyboard and we click on "a" now the keyboard sends 01011001 to the CPU and come back as a output on the monitor. 
Output Device. I am sitting in a voice chat on discord. When i say something in my microphone it will come out as a output on my friends PC.



*What is sequential execution?


Doing the first task then the next and the next ....


*Which 4 things should you do when debugging?


1. Reading
2. Running
3. Ruminating (What kind of error)
4. Retreating
