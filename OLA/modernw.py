import asyncio
import callofduty
from callofduty import Mode, Platform, Title
from easygui import passwordbox

username = passwordbox("Enter your Blizzard email:")        # Opens a new window to enter username
password = passwordbox("Enter your password:")              # Opens a new window to enter password

async def cod():

    client = await callofduty.Login(username, password)     # login information

    while True:                                                                                 # if there is connection go to next line in code

        results = await client.SearchPlayers(Platform.Activision, "Captain Price", limit=3)     # Searching for the user on the client with 3 requests
        for player in results:
            print(f"{player.username} ({player.platform.name})")

        me = results[1]
        profile = await me.profile(Title.ModernWarfare, Mode.Multiplayer)                       # Looking at stats from game: Modernwarfare and gamemode: Multiplayer

        level = profile["level"]                                                                # Looking at the k/d ratio and w/l ratio
        kd = profile["lifetime"]["all"]["properties"]["kdRatio"]
        wl = profile["lifetime"]["all"]["properties"]["wlRatio"]

        print(f"\n{me.username} ({me.platform.name})")                                          # Prints the level, k/d ratio and w/l ratio
        print(f"Level: {level}, K/D Ratio: {kd}, W/L Ratio: {wl}")
        break



asyncio.get_event_loop().run_until_complete(cod())                                              # runs the code until every needed stats are done.




