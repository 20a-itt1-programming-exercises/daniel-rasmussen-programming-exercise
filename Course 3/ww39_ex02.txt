1.What is the difference between conditional execution and alternative execution?

* The conditional execution have the ability to check conditions and also change the behavior of the program.

For instance:
 if y > 0 :
      print('x is positive')

The boolean expression after if statement is the definition of condition.
Always use (:) after a if statement

* The alternative execution can be recognized with the two possibilities and the condition determines which one gets executed.

For instance:
 if x%2 == 0 :
      print ('x is even')
else : 
      print ('x is odd')

The outer conditional contains two branches. The first branch contains a simple
statement. The second branch contains another if statement, which has two
branches of its own. Those two branches are both simple statements, although
they could have been conditional statements as wel

2.What is the purpose of chained conditionals and what is the syntax for nested conditionals?



3.What are exeptions in Python and how can you handle them ? Use examples.

inp = input('Enter Fahrenheit Temperature:')
try:
    fahr = float(inp)
    cel = (fahr - 32.0) * 5.0 / 9.0
    print(cel)
except:
     print('Please enter a number')
If this code is running, then we have to enter the Fahrenheit temp like this:
Enter Fahrenheit Tempereature:
50
Then it will calculate the degrees in celsius. But if we write with letters it will say, that you have to enter a number, and NO letters.
