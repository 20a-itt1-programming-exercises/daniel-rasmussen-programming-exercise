#Exercise 3: Write a program to prompt for a score between 0.0 and
#1.0. If the score is out of range, print an error message. If the score is
#between 0.0 and 1.0, print a grade using the following table:

# Score grade:
# >= 0.9 A
# >= 0.8 B
# >= 0.7 C
# >= 0.6 D
# < 0.6 F
try:        # If the input isn't between 0-1 the output will be "Bad score"
    Grade = float(input("Enter grade between 0-1:\n"))             # Enter a number between 0-1

    if Grade >= 0.9:                                               # The output will be an A if Grade is equal or higher than 0.9
        print("Perfekt! You got an A")
    else:
        if Grade >= 0.8:                                           # The output will be a B if Grade is equal or higher than 0.8
            print("Nice! You got a B")
        else:
            if Grade >= 0.7:                                       # The output will be a C if Grade is equal or higher than 0.7
                print("Good! You got a C")
            else:
                if Grade >= 0.6:                                   # The output will be a D if Grade is equal or higher than 0.6
                    print("Okay! You got a D")
                else:
                    if Grade < 0.6:                                # The output will be a F if Grade is lower than 0.6
                        print("Try again! You got a F")
except:                                                            # If the input isn't between 0-1 the output will be "Bad score"
    print("Bad score")




