#Rewrite your pay program using try and except so that your
#program handles non-numeric input gracefully by printing a message
#and exiting the program. The following shows two executions of the
#program:

#Enter Hours: 20
#Enter Rate: nine
#Error, please enter numeric input

#Enter Hours: forty
#Error, please enter numeric input

try:                                                        # if the answer for the input is written with a letter it will stop!
    hours = int(input("Enter hours:\n"))                    # insert a value for hours
    rate = float(input("Enter rate:\n"))                    # insert a value for rate
except :                                                    # stop running the code if the answer contains letters
    print("STOP! Numbers only!")                            # prints a message if the code stops
    quit()
if hours > 40:                                              # adds 1.5x the rate after 40 hours of work
    pay = (hours-40) * rate * 1.5 + 40 * rate
    print("You have worked more than 40 hours this weak and will get 1.5x more hourly rate. Instead of",pay,"you will get",pay*1.5,"before taxes")
else :                                                      # general calculation of salary
    pay = hours * rate
    print("You will get payed:\n", pay,"dkk", "if you work", hours, "hours","with a rate of", rate, "dkk/h")





