#Exercise 1: Rewrite your pay computation to give the employee 1.5
#times the hourly rate for hours worked above 40 hours


hours = int(input("Enter hours:\n"))            # insert a value for hours
rate = float(input("Enter rate:\n"))            # insert a value for rate
pay = hours * rate                              # calculation for pay
if hours > 40:                                  # # adds 1.5x the rate after 40 hours of work
    print("You have worked more than 40 hours this weak and will get 1.5x more hourly rate. Instead of",pay,"you will get",pay*1.5,"before taxes")
else :                                          # gives the pay without extra rate
    print("You will get payed:\n", pay, "if you work",hours,"hours","with a rate at",rate,"before taxes")