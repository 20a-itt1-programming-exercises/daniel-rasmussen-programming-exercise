#Exercise 5: What will the following Python program print out?
#a) Zap ABC jane fred jane
#b) Zap ABC Zap
#c) ABC Zap jane
#d) ABC Zap ABC
#e) Zap Zap Zap

def fred():             # defines a function called "fred"
    print("Zap")        # execute Zap


def jane():             # defines a function called "jane"
    print("ABC")        # execute ABC

jane()
fred()
jane()

# the following program will print:
# d) ABC Zap ABC
