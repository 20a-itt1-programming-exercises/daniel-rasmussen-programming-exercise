#Exercise 7: Rewrite the grade program from the previous chapter using
#a function called computegrade that takes a score as its parameter and
#returns a grade as a string.

def computergrade(Grade):
    if Grade < 0 or Grade > 1:                                # If the input isn't between 0-1 the output will be "Bad score"
        print("Please enter a number between 0 - 1")
    elif Grade >= 0.9:                                        # The output will be an A if Grade is equal or higher than 0.9
        print("Perfekt! You got an A")
    elif Grade >= 0.8:                                        # The output will be a B if Grade is equal or higher than 0.8
        print("Nice! You got a B")
    elif Grade >= 0.7:                                        # The output will be a C if Grade is equal or higher than 0.7
        print("Good! You got a C")
    elif Grade >= 0.6:                                        # The output will be a D if Grade is equal or higher than 0.6
        print("Try again! You got a D")
    elif Grade < 0.6:                                         # The output will be a F if Grade is lower than 0.6
        print("Try again! You got a F")
try:                                                          # If the input isn't between 0-1 the output will be "Enter a grade between 0 - 1"
    Grade = float(input("Enter grade between 0 - 1:\n"))      # Enter a number between 0-1
except:
    print("Please enter a number between 0 - 1")
    quit()

print(computergrade(Grade))                                   # prints the output as a Grade.


