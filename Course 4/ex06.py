#Exercise 6: Rewrite your pay computation with time-and-a-half for overtime and create a function called computepay which takes two parameters
#(hours and rate).
#Enter Hours: 45
#Enter Rate: 10
#Pay: 475.0

def computepay(hours, rate):                            # defines a function with the varible values of hours and rate
    if hours > 40:                                      # if the person worked for more than 40 hours there will be a 1.5x bonus rate
        pay = (hours-40) * rate * 1.5 + 40 * rate
    else:
        pay = hours * rate
    return pay

try:
    hours = int(input("Enter hours:\n"))            # insert a value for hours
    rate = float(input("Enter rate:\n"))            # insert a value for rate
    pay = computepay(hours, rate)                   # calculation for pay
    print("You will get payed:", pay)
    print("After taxes you will get:", pay*0.38*0.92)
except:
    print("Numbers only!")