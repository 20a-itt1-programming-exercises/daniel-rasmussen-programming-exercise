#Exercise 2: Move the last line of this program to the top, so the function
#call appears before the definitions. Run the program and see what error
#message you get.

#repeat_lyrics()                                    # can't run because it is nessecery to create a function before it can be executed

def print_lyrics():                                 # defines a function called: "print_lyrics"
    print("I'm a lumberjack, and I'm okay.")
    print('I sleep all night and I work all day.')
def repeat_lyrics():                                # defines a function called: "print_lyrics"
    print_lyrics()
    print_lyrics()

repeat_lyrics()


# if the line: "repeat_lyrics()" is in the top of the program the error will be: "name 'repeat_lyrics' is not defined"